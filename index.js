import { connect } from 'mongoose' 
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel' // import del Model che permette la creazione di Documents con i dati degli utenti
import cors from 'cors'

//Connessione al database
connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true }) 

//Express framework
const app = express()

//Middleware per impostare l'header Access-Control-Allow-Origin che abilita il cross origin resource sharing cos� da non bloccare le richieste al server da altri siti
app.use(cors())
//Middleware per effettuare il parsing delle richieste ricevute in json e urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

//Endpoint con url '/' che accetta richieste GET e risponde con 'Just a test'
app.get('/', (req, res) => { 
  res.send('Just a test')
})

//Endpoint con url '/users' che accetta richieste GET e alla richiesta effettua una query async verso UserModel e risponde inviando tutti i documents di UserModel  
app.get('/users', (req, res) => { 
  UserModel.find((err, results) => {
    res.send(results)
  })
})

//Endpoint con url 'users' che accetta richieste POST creando un nuovo document ad ogni richiesta accettando come valori gli input inviati dall'utente con la richiesta POST
//Se la registrazione ha avuto successo viene inviato in risposta il contenuto del nuovo document, in caso di errore viene inviato il messaggio di errore
app.post('/users', (req, res) => {
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

app.put('/:id', (req, res) => {
  let id = req.params.id
  
  UserModel.findByIdAndUpdate(id, {
    email: req.body.email,
    firstName = req.body.firstName,
    lastName = req.body.lastName,
    password = req.body.password
  }, (err, updatedDoc) => {
      err ? res.send(err) : updatedDoc.save((err, updatedUser) => {
       err ? res.send(err) : res.send(updatedUser)
    })
  })
}

app.delete('/:id', (req, res) => {
  let id = req.params.id
  
  UserModel.findByIdAndDelete(id, (err, deletedUser) => {
   err? res.send(err) : res.send(deletedUser)
  })
}

//impostazione della PORT dalla quale si possono ricevere richieste e si inviano le risposte
app.listen(8080, () => console.log('Example app listening on port 8080!'))
