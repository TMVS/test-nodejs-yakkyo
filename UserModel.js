import { Schema, model } from 'mongoose'

//Definizione di uno schema per gli utenti con le specificazioni e i requisiti per ogni field
const UserSchema = new Schema({
  email: { type: String, lowercase: true, trim: true, required: true, unique: true },
  password: { type: String, required: true, select: false },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true }
})

//Esportazione del Model compilato dallo UserSchema
export default model('User', UserSchema)
